<!DOCTYPE html>
<html>
  <head>
    <title>Instagram mapper</title>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <div id="map"></div>
    <button id="button">Back to world</button>
  </body>
  <script type="text/javascript">
    function initMap() {
      const size = new google.maps.Size(50, 50);
      const map = new google.maps.Map(
        document.getElementById('map'),
        { zoom: 3, center: {lat: 30, lng: 0} }
      );
      const button = document.getElementById('button');
      button.onclick = function() {
        map.setZoom(3);
        map.setCenter({lat: 30, lng: 0});
      };

      <?php
        require_once 'model.php';
        $api = new InstagramAPIWrapper();
        $api->printMarkers();
      ?>
    }
  </script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBEa7ZIoHFOXCoPP-_IibPExglZY72D3Pw&callback=initMap">
  </script>
</html>