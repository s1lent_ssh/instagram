<?php

set_time_limit(0);
date_default_timezone_set('UTC');
require '../vendor/autoload.php';

class UserLocationsInfo {
  public $storyPhoto;
  public $storyVideo;
  public $user;
  public $locations;
}

class InstagramAPIWrapper {

	private $username = '';
	private $password = '';
	private $debug = false;
	private $truncatedDebug = false;

	function __construct() {
		\InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
		$this->ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

		try {
    		$this->ig->login($this->username, $this->password);
		} catch (\Exception $e) {
    		echo $e->getMessage()."\n";
    		exit(0);
		}
	}
 
	private function getUsersWithStories() {
    	$trayArray = $this->ig->story->getReelsTrayFeed()->getTray();
    	$users = array();
    	foreach ($trayArray as $tray) {
    		$u_id = $tray->getUser()->getPk();
    		$reel = $this->ig->story->getUserStoryFeed($u_id)->getReel();
    		foreach ($reel->getItems() as $item) {
    			$locations = $item->getStoryLocations();
    			//getVideoVersions()

    			if(!is_null($locations)) {
    				$userInfo = new UserLocationsInfo();
    				$userInfo->user = $tray->getUser();
    				$userInfo->locations = $locations;
            $userInfo->storyPhoto = $item->getImageVersions2()->getCandidates()[0]->getUrl();


            $videoVersions = $item->getVideoVersions();
            if(!is_null($videoVersions)) {
              $userInfo->storyVideo = $videoVersions[0]->getUrl();;
            }

    				array_push($users, $userInfo);
    			}
    		}
        usleep(1000 * 300);
    	}
    return $users;
	}

	function printMarkers() {
		try {
          $users = $this->getUsersWithStories();
          foreach ($users as $userLocationInfo) {
            foreach ($userLocationInfo->locations as $location_meta) {
              $user = $userLocationInfo->user;
              $location = $location_meta->getLocation();
              $video = $userLocationInfo->storyVideo;
              $photo = $userLocationInfo->storyPhoto;
              echo 
                  'new google.maps.Marker({'.
                    'position: {lat:'.$location->getLat().', lng: '.$location->getLng().'},'.
                    'icon: {url: "'.$user->getProfilePicUrl().'", scaledSize: size },'.
                    'map: map'.
                  '}).addListener("click", function() { 
                  		//map.setZoom(15);
                  		map.setCenter({lat:'.$location->getLat().', lng: '.$location->getLng().'});
                  		var infoWindow = new google.maps.InfoWindow({
        					       content: \'<video src="'.$video.'" controls></video>\'
      					      });
      					      infoWindow.open(map, this);
                  });';
            } // foreach locations
          } //foreach users
        } catch (Exception $e) {
          echo $e->getMessage();
          exit(0);
        }
	}

}

?>